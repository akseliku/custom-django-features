import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="django-custom-features",
    version="0.0.2",
    author="Akseli Kuistio",
    description="Django tools",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/akseliku/custom-django-features",
    packages=setuptools.find_packages(exclude=('tests',)),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: Linux",
    ],
    install_requires=[
          'django-model-utils',
    ],
    python_requires='>=3.6',
)