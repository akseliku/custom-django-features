from abc import ABC
from datetime import datetime
from unittest import mock

from django import db
from django.db import DatabaseError
from django.db.models import DateTimeField, AutoField, Model

from django_custom_features.model import ModelMixin
from django_custom_features.testing import ModelTools, ExtraAssertions


class Timestamps(ModelMixin, ABC):
    test_message = None

    @staticmethod
    def class_attributes(attrs, name, **kwargs):
        attrs['created'] = DateTimeField(auto_now_add=True)
        attrs['modified'] = DateTimeField(auto_now=True)

    @classmethod
    def class_init(cls):
        cls.test_message = 'created/modified'


class TimestampReplace(Timestamps, ABC):
    test_message = None
    test_message_replacement = None

    @staticmethod
    def class_attributes(attrs, **kwargs):
        attrs['created_at'] = DateTimeField(auto_now_add=True)
        attrs['modified_at'] = DateTimeField(auto_now=True)

    @classmethod
    def class_init(cls):
        cls.test_message_replacement = 'created+modified'
        

class TimestampAppend(TimestampReplace, ABC):
    test_message_replacement = None

    @staticmethod
    def class_attributes(attrs, super_attributes, **kwargs):
        super_attributes()
        attrs['deleted_at'] = DateTimeField(null=True)

    @classmethod
    def class_init(cls):
        super().class_init()
        cls.test_message_replacement += '+deleted'


class ClassIsTableNameUpper(ModelMixin, ABC):
    just_a_simple_init_var = None

    @staticmethod
    def class_attributes(name, meta, **kwargs):
        meta.db_table = name.upper()

    @classmethod
    def class_init(cls):
        cls.just_a_simple_init_var = 'OK'


class Pointless(ModelMixin, ABC):
    test_var = None

    @classmethod
    def class_init(cls):
        cls.test_var = 'OK'

class Test1(ModelMixin, ABC):
    test_var = None

    @staticmethod
    def class_attributes(meta, name, **kwargs):
       meta.db_table = name.lower() + '1'

class Test2(ModelMixin, ABC):
    test_var = None

    @staticmethod
    def class_attributes(meta, name, **kwargs):
        meta.db_table += name.lower() + '2'

class Test3(ModelMixin, ABC):
    @staticmethod
    def class_attributes(meta, name, **kwargs):
        meta.db_table += name.lower() + '3'

class Test4(ModelMixin, ABC):
    @staticmethod
    def class_attributes(meta, name, **kwargs):
        meta.db_table += name.lower() + '4'




class TestModelMixins(ModelTools, ExtraAssertions):
    # #
    # Setup:

    def mocked_datetime(self):
        return self.now

    def setUp(self):
        self.now = datetime.now()
        self.datetime_patch = mock.patch('django.utils.timezone.now', autospec=True)
        self.datetime_mock = self.datetime_patch.start()
        self.datetime_mock.side_effect = self.mocked_datetime

    def tearDown(self):
        self.datetime_patch.stop()

    # #
    # Tests:

    class ModelA(Model, Timestamps):
        class Meta:
            app_label = 'test'
        id = AutoField(primary_key=True)

    def test_attributes_and_init_are_applied(self):
        self.now = datetime(2020, 1, 1, 0, 0, 0, 100)
        a = self.ModelA.objects.create()
        self.assertMatchesModel({'id': a.pk, 'created': self.now, 'modified': self.now}, self.ModelA)
        self.assertEqual(self.ModelA.test_message, 'created/modified')

    class ModelB(Model, TimestampReplace):
        class Meta:
            app_label = 'test'
        id = AutoField(primary_key=True)

    def test_attributes_and_init_are_replaced(self):
        self.now = datetime(2020, 1, 1, 0, 0, 0, 100)
        b = self.ModelB.objects.create()
        self.assertFalse(hasattr(b, 'created'))
        self.assertFalse(hasattr(b, 'modified'))
        self.assertMatchesModel({'id': b.pk, 'created_at': self.now, 'modified_at': self.now}, self.ModelB)
        self.assertIsNone(self.ModelB.test_message)  # Should be null as super().class_init not called in override
        self.assertEqual(self.ModelB.test_message_replacement, 'created+modified')

    class ModelC(Model, TimestampAppend):
        class Meta:
            app_label = 'test'
        id = AutoField(primary_key=True)

    def test_attributes_and_init_can_be_inherited(self):
        self.now = datetime(2020, 1, 1, 0, 0, 0, 100)
        c = self.ModelC.objects.create()
        self.assertFalse(hasattr(c, 'created'))
        self.assertFalse(hasattr(c, 'modified'))
        self.assertMatchesModel(
            {'id': c.pk, 'created_at': self.now, 'modified_at': self.now, 'deleted_at': None},
            self.ModelC
        )
        self.assertIsNone(self.ModelC.test_message)
        self.assertEqual(self.ModelC.test_message_replacement, 'created+modified+deleted')

    class ModelD(Model, TimestampAppend, ClassIsTableNameUpper):
        class Meta:
            app_label = 'test'

        id = AutoField(primary_key=True)

    def test_multiple_inheritance(self):
        self.now = datetime(2020, 1, 1, 0, 0, 0, 100)
        d = self.ModelD.objects.create()

        self.assertMatchesModel(
            {'id': d.pk, 'created_at': self.now, 'modified_at': self.now, 'deleted_at': None},
            self.ModelD
        )
        self.assertEqual(self.ModelD.test_message_replacement, 'created+modified+deleted')

        self.assertEqual(d._meta.db_table, 'MODELD')
        self.assertEqual(self.ModelD.just_a_simple_init_var, 'OK')

    class ModelAbstract(Model, TimestampAppend, ClassIsTableNameUpper):
        class Meta:
            abstract = True

    class ModelChild(ModelAbstract):
        class Meta:
            app_label = 'test'

        id = AutoField(primary_key=True)

    def test_using_an_abstract_baseclass(self):
        self.now = datetime(2020, 1, 1, 0, 0, 0, 100)
        child = self.ModelChild.objects.create()

        self.assertMatchesModel(
            {'id': child.pk, 'created_at': self.now, 'modified_at': self.now, 'deleted_at': None},
            self.ModelChild
        )
        self.assertEqual(self.ModelChild.test_message_replacement, 'created+modified+deleted')

        self.assertEqual(child._meta.db_table, 'MODELCHILD')
        self.assertEqual(self.ModelChild.just_a_simple_init_var, 'OK')

    class ModelChildB(ModelAbstract, Pointless):
        class Meta:
            app_label = 'test'

        id = AutoField(primary_key=True)

    def test_using_combination_of_abstract_base_and_direct_mixin(self):
        self.now = datetime(2020, 1, 1, 0, 0, 0, 100)
        child = self.ModelChildB.objects.create()

        self.assertMatchesModel(
            {'id': child.pk, 'created_at': self.now, 'modified_at': self.now, 'deleted_at': None},
            self.ModelChildB
        )
        self.assertEqual(self.ModelChildB.test_message_replacement, 'created+modified+deleted')

        self.assertEqual(child._meta.db_table, 'MODELCHILDB')
        self.assertEqual(self.ModelChildB.just_a_simple_init_var, 'OK')
        self.assertEqual(self.ModelChildB.test_var, 'OK')

    class OrderA(Model, Test1, Test2, Test4, Test3):
        class Meta:
            app_label = 'test'

    class OrderAbstract(Model, Test1, Test2, Test4, Test3):
        class Meta:
            abstract = True
            app_label = 'test'

    class OrderB(OrderAbstract):
        class Meta:
            app_label = 'test'

    def test_mixins_applied_in_correct_order(self):
        self.assertEqual(self.OrderA._meta.db_table, 'ordera1ordera2ordera4ordera3')
        self.assertEqual(self.OrderB._meta.db_table, 'orderb1orderb2orderb4orderb3')

    # todo: enforce class/attribute initialization only in abstract mixins, or handle in concrete model

    class BadModel(Timestamps):
        class Meta:
            app_label = 'test'
        id = AutoField(primary_key=True)

    def test_must_explicitly_inherit_a_model(self):
        tables = db.connection.introspection.table_names()

        self.assertNotIn(self.BadModel._meta.db_table, tables)
        with self.assertRaises(TypeError):
            self.BadModel()
        with self.assertRaises(TypeError):
            self.BadModel.objects.create()
        with self.assertRaises(DatabaseError):
            self.BadModel.objects.first()
