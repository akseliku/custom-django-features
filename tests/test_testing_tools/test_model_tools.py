from django import db
from django.db.models import Model, AutoField, CharField, DateTimeField, ForeignKey, CASCADE, IntegerField

from django_custom_features.testing import ModelTools


class TestingModel(Model):
    class Meta: app_label = 'tests'

    id = AutoField(primary_key=True)
    text = CharField(max_length=50)
    created = DateTimeField(auto_now_add=True)


class TestModelCreation(ModelTools):
    create_test_models = [TestingModel]

    class TestingRelation(Model):
        class Meta: app_label = 'tests'

        id = AutoField(primary_key=True)
        text = CharField(max_length=50)
        created = DateTimeField(auto_now_add=True)

        relation = ForeignKey(TestingModel, on_delete=CASCADE, related_name='related')

    def test_model_class_is_created(self):
        # Model defined outside test and explicitly created:
        m = TestingModel.objects.create(text='ABC')
        self.assertEqual(TestingModel.objects.count(), 1)

        # Model defined nested in test class and automatically created:
        self.TestingRelation.objects.create(text='QWE', relation=m)
        self.assertEqual(self.TestingRelation.objects.count(), 1)

        # Also check relation is working:
        m = TestingModel.objects.first()
        self.assertEqual(m.related.first().text, 'QWE')

    class TestingAbstract(Model):
        class Meta:
            app_label = 'tests'
            db_table = 'test_abstract'
            abstract = True

        id = AutoField(primary_key=True)
        created = DateTimeField(auto_now_add=True)

    def test_abstract_is_not_created(self):
        tables = db.connection.introspection.table_names()
        self.assertNotIn('test_abstract', tables)
        self.assertNotIn('tests_testingabstract', tables)  # Django default naming scheme just in case
