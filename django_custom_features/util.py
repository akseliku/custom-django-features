from typing import Hashable, List, Sequence


def columns(data: Sequence, columns: Sequence):
    return [tuple(row[column] for column in columns) for row in data]


class RangeDict(dict):
    def __init__(self):
        self._locked = False
        super().__init__()

    def __setitem__(self, key, v):
        key = key if type(key) == tuple else (key,)
        for k in key:
            if type(k) == slice:
                for i in range(k.start, k.stop+1, k.step if type(k.step) == int else 1):
                    super().__setitem__(i, v)
            else:
                super().__setitem__(k, v)

    def __getitem__(self, key):
        return \
            super().__getitem__(key) \
            if key in self or ... not in self else \
            super().__getitem__(...)

    def get(self, k, *args):
        use_default = bool(args)
        try:
            return self[k]
        except (KeyError if use_default else None):
            return args[0]


def set_dict_path(d, path, value, sep='.'):
    """E.g. set_dict_path(d, 'a,b,c', 1, sep=',') is equivalent to d['a']['b']'[c'] = 1, but will also generate the
    nested dictionaries as necessary."""
    path = path.split(sep)
    node = d
    for i, k in enumerate(path[:-1]):
        node = node.setdefault(k, {})
        if not isinstance(node, dict):
            raise ValueError(
                "Nested dictionary node %s is not a dict, cannot add key '%s'"
                % (''.join('[%s]' % k for k in path[:i+1]), path[i+1])
            )
    node[path[-1]] = value


def flatten_dict(d, sep='.', isep='[%d]'):
    def _add_sep(k):
        return isep % k if type(k) == int else sep + k
    def _recurse(prefix, node):
        if isinstance(node, list) and len(node):
            node = dict(enumerate(node))
        if isinstance(node, dict):
            ret = []
            [ret.extend(_recurse(prefix + _add_sep(k), v)) for k, v in node.items()]
            return ret
        else:
            return [(prefix, node,)]
    return {k.strip('.'): v for k, v in _recurse('', d)}


def find_or_create(rows, key, value, defaults=None) -> dict:
    """ Find first row containing value in a list of dicitonaries. If not found creates a new row.

    Args:
        rows (List[dict]): List to search.
        key (Hashable):
        value (Any):
        defaults (dict): Default values if new row created (in addition to searched key-value).

    Returns:
        A row by reference, containing key-value.
    """
    defaults = defaults or {}
    defaults.setdefault(key, value)
    row = next(
        (row for row in rows if row[key] == value), None
    ) or rows.append(defaults) or rows[-1]
    return row


def new_class(*base_classes):
    class cls(*base_classes):
        pass
    return cls
