import json
from datetime import datetime
from inspect import isclass
from json import JSONDecodeError
from typing import Sequence, Union, Type

from django.db import connection
from django.db.models import Model, DateTimeField, ManyToOneRel
from django.forms import model_to_dict
from django.test import TestCase

from django_custom_features.util import columns, flatten_dict


class ModelTools(TestCase):
    create_test_models = []

    @classmethod
    def setUpClass(cls):
        models = [attr for attr in cls.__dict__.values() if isclass(attr) and issubclass(attr, Model)]
        models += cls.create_test_models
        cls._add_test_models(models)
        super().setUpClass()

    @classmethod
    def _add_test_models(cls, model_classes):
        tables = connection.introspection.table_names()
        with connection.schema_editor() as schema_editor:
            for model_cls in model_classes:
                table_name = model_cls._meta.db_table
                if model_cls._meta.db_table in tables:
                    raise EnvironmentError("Cannot create test table '%s', table already exists" % table_name)
                if not model_cls._meta.abstract:
                    schema_editor.create_model(model_cls)


class ExtraAssertions(TestCase):
    def assertColumn(self, actual: Sequence[dict], expected: Sequence, column: Union[str, Sequence], msg=None):
        def is_collection(instance):
            return isinstance(instance, Sequence) and type(instance) != str

        if not is_collection(column):
            column = (column,)

        expected = [tuple(value) if is_collection(value) else (value,) for value in expected]

        actual_values = columns(actual, column)
        missing = [','.join(str(v) for v in value) for value in expected if value not in actual_values]
        extra = [','.join(str(v) for v in value) for value in actual_values if value not in expected]
        msg_list = ['Column (%s) is not equal:' % ','.join(column)]
        if missing:
            msg_list += ['\tmissing values: (' + '), ('.join(missing) + ')']
        if extra:
            msg_list += ['\textra values: (' + '), ('.join(extra) + ')']
        if msg:
            msg_list += [msg]
        if missing or extra:
            raise AssertionError('\n'.join(msg_list))

    def assertMatchesModel(self, data: dict, model_cls: Type[Model], model_id=None, **kwargs):
        is_sub_assertion = 'diffs' in kwargs
        diffs = kwargs.get('diffs', {})

        def raise_or_append(msg, field=''):
            nonlocal diffs
            if is_sub_assertion:
                diffs[field] = msg
            else:
                raise AssertionError(msg)

        def matches(m: Model, k, v):
            nonlocal diffs
            field = m._meta.get_field(k)
            msg = "%s != %s" % (v, getattr(m, k))
            if isinstance(field, DateTimeField) and None not in (v, getattr(m, k)):  # ISO datetime string
                try:
                    if isinstance(v, datetime):
                        v = v.timestamp()
                    if type(v) not in (float, int):
                        v = datetime.strptime(v, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp()
                    if getattr(m, k).timestamp() != v:
                        diffs[k] = msg
                except ValueError:
                    diffs[k] = msg
            elif isinstance(getattr(m, k), Model):  # Relationship
                if not isinstance(v, dict) or getattr(m, k).pk != v.get(getattr(m, k)._meta.pk.name, not m.pk):
                    diffs[k] = "%s != %s" % (v, model_to_dict(getattr(m, k)))
            elif isinstance(field, ManyToOneRel):  # Reverse relationship
                sub_diffs = {}
                self.assertMatchesModels(v, field.related_model, diffs=sub_diffs)
                if len(sub_diffs):
                    diffs[k] = sub_diffs
            elif getattr(m, k) != v:
                if str(getattr(m, k)) == str(v):
                    diffs[k] = "%s(%s) != %s(%s)" % (type(v).__name__, v, type(getattr(m, k)).__name__, getattr(m, k))
                else:
                    diffs[k] = msg

        pk = model_cls._meta.pk.name
        name = model_cls.__name__
        model_id = model_id or isinstance(data, dict) and data.get(pk, None) or None
        if not model_id:
            return raise_or_append("Primary key '%s' of %s not present in data." % (pk, name))
        try:
            model = model_cls.objects.filter(pk=model_id).first()
        except ValueError:
            model = None
        if not model:
            return raise_or_append("%s(%s) does not exist." % (name, model_id))

        [matches(model, k, v) for k, v in data.items()]
        if diffs and not is_sub_assertion:
            diffs = flatten_dict(diffs)
            diffs = ['%s: %s' % (k, v) for k, v in diffs.items()]
            raise AssertionError("Mismatching values:\n\t" + '\n\t'.join(diffs))

    def assertMatchesModels(self, data: list, model_cls: Type[Model], model_id=None, **kwargs):
        is_sub_assertion = 'diffs' in kwargs
        diffs = kwargs.get('diffs', {})
        for i, row in enumerate(data):
            row_diff = {}
            self.assertMatchesModel(row, model_cls, model_id, diffs=row_diff)
            if len(row_diff):
                diffs[i] = row_diff
        if diffs and not is_sub_assertion:
            msg = 'Mismatching values:\n\t'
            for row, row_diff in diffs.items():
                row_err = row_diff.pop('', '')
                if row_err:
                    msg += 'Row %d: %s\n\t' % (row, row_err)
                else:
                    msg += 'Row %d:\n\t\t' % row + '\n\t\t'.join('%s: %s' % (k, v) for k, v in flatten_dict(row_diff).items())

            raise AssertionError(msg)

    def assertPartialDict(self, expected: dict, full_set: dict, msg_id: str=None):
        msg_id = '' if not msg_id else ' (%s)' % msg_id
        if not isinstance(full_set, dict):
            raise AssertionError("%s is not dict%s" % (type(full_set).__name__, msg_id))

        differences = '\n\t'.join(
            "%s: %s %s" % (k, v, ('!= %s' % full_set[k]) if k in full_set else 'not set')
            for k, v in expected.items() if full_set.get(k, not v) != v
        )
        if differences:
            raise AssertionError("Mismatching values%s:\n\t" % msg_id + differences)

    def assertPartialDicts(self, expected: list, full_sets: list):
        if len(expected) != len(full_sets):
            raise AssertionError('Number of dicts: %s, expected %s' % (len(full_sets), len(expected)))
        messages = []
        for i, (row, full_set) in enumerate(zip(expected, full_sets)):
            try:
                self.assertPartialDict(row, full_set, msg_id='on row %s' % i)
            except AssertionError as e:
                messages.append(str(e))
        if messages:
            messages = '\n'.join(messages).replace('\n', '\n\t')
            raise AssertionError('Lists of dicts mismatched:\n\t' + messages)

    def assertListEqual(self, list1, list2, sort=False, msg=None):
        if sort:
            list1.sort(), list2.sort()
        return super().assertListEqual(list1, list2, msg)


class RequestHelpers(TestCase):
    def _request(self, response, content_key):
        try:
            if content_key is None:
                return response, json.loads(response.content) if response.content else None
            else:
                return response, json.loads(response.content).get(content_key, None)
        except JSONDecodeError:
            return response, None

    def delete(self, route, content_key='result'):
        response = self.client.delete(route, content_type='application/json')
        return self._request(response, content_key)

    def get(self, route, content_key='result'):
        response = self.client.get(route)
        return self._request(response, content_key)

    def post(self, route, data, content_key='result'):
        response = self.client.post(route, data, 'application/json')
        return self._request(response, content_key)

    def put(self, route, data, content_key='result'):
        response = self.client.put(route, data, 'application/json')
        return self._request(response, content_key)

    def patch(self, route, data, content_key='result'):
        response = self.client.patch(route, data, 'application/json')
        return self._request(response, content_key)