from abc import abstractmethod

from django.apps import apps
from django.core.management import call_command
from django.db import connection
from django.test.runner import DiscoverRunner


class PsqlTestSchemaRunner(DiscoverRunner):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.psql_test_options = '-c search_path=unittest'

    def setup_test_environment(self, **kwargs):
        # Force Django to connect to the correct db for tests
        db_conf = connection.settings_dict
        test_conf = db_conf['TEST']
        test_conf['NAME'] is None and test_conf.pop('NAME')
        test_conf.setdefault('OPTIONS', {})

        if 'SCHEMA' in test_conf:
            schema_name = test_conf.pop('SCHEMA')
            self.psql_test_options = '-c search_path=%s' % schema_name
            test_conf['OPTIONS']['options'] = self.psql_test_options
            if schema_name.lower() == 'public':
                raise EnvironmentError("Cannot use 'public' as the test schema")

        db_conf.update(test_conf)
        connection.connect()
        super().setup_test_environment(**kwargs)

    def setup_databases(self, **kwargs):
        # Introspection:
        models = apps.get_models()
        tables = connection.introspection.table_names()
        engine = connection.settings_dict.get('ENGINE', '').split('.')[-1]
        options = connection.settings_dict.get('OPTIONS', {}).get('options', None)

        # Ensure test database in use:
        psql_engines = ['postgresql_psycopg2', 'django_postgrespool', 'django_postgrespool2']
        if engine not in psql_engines:
            raise EnvironmentError("PsqlTestSchemaRunner must be used with a postgres engine")
        if options != self.psql_test_options:
            raise EnvironmentError("Wrong test schema")
        if tables:
            raise EnvironmentError("Test schema is not empty")

        # To copy production database instead of using framework schemas: todo
        # with connection.schema_editor() as schema_editor:
        #     with connection.cursor() as cursor:
        #         for Unmanaged in [m for m in models if not m._meta.managed]:
        #             db_table = Unmanaged._meta.db_table
        #             cursor.execute('CREATE TABLE "%s" (LIKE public.%s INCLUDING ALL)' % (db_table, db_table))

        # Create unmanaged models:
        with connection.schema_editor() as schema_editor:
            for Unmanaged in [m for m in models if not m._meta.managed]:
                schema_editor.create_model(Unmanaged)

        # Django migrations:
        call_command("migrate", interactive=False, verbosity=0)

    def teardown_databases(self, old_config, **kwargs):
        tables = connection.introspection.table_names()
        for table in tables:
            connection.cursor().execute('DROP TABLE IF EXISTS "%s" CASCADE' % table)


