import re
from abc import ABC, ABCMeta, abstractmethod
from typing import List, Type

# from django.core.exceptions import FieldDoesNotExist
# from django.db.models import Model, QuerySet,
from django.db.models.base import ModelBase, Model
from model_utils import FieldTracker

# from .events import EventHooks, event
# from .manager import DBModelManager, DBAuthModelManager
# from .serializer import serializer_factory, DBModelSerializer
# from ..util import RangeDict
from django_custom_features.util import new_class


class BaseMeta(ModelBase, ABCMeta):
    models = []  # type: List[ModelBase]

    def __new__(mcs, name, bases, attrs):
        is_model_subclass = any((issubclass(base, Model) for base in bases))

        # Ensure Meta attribute:
        if 'Meta' not in attrs:
            class Meta: pass
            attrs['Meta'] = Meta

            if not is_model_subclass:
                attrs['Meta'].abstract = True

        # Check class is not an abstract model before applying features:
        is_concrete_subclass = is_model_subclass and not getattr(attrs['Meta'], 'abstract', False)

        if is_concrete_subclass:
            meta = attrs['Meta']
            meta.base_manager_name = 'objects'  # todo: is excplicit definition required?

            # Generate the 'super_attributes' convenience function:
            def super_attributes_factory(bases):
                nonlocal attrs, name, meta
                def super_attributes():
                    [
                        base.class_attributes(
                            attrs=attrs, name=name, meta=meta, super_attributes=super_attributes_factory(base.__bases__)
                        )
                        for base in bases if issubclass(base, ModelMixin)
                    ]
                return super_attributes

            # Invoke 'class_attributes' from mixins:
            invoked = set()
            def invoke(base):
                nonlocal attrs, name, meta, bases
                if 'class_attributes' not in base.__dict__:
                    [invoke(b) for b in base.__bases__ if issubclass(base, ModelMixin)]
                elif base not in invoked:
                    invoked.add(base)
                    base.class_attributes(
                        attrs=attrs, name=name, meta=meta, super_attributes=super_attributes_factory(bases)
                    )
            [invoke(base) for base in bases if issubclass(base, ModelMixin)]

            cls = super().__new__(mcs, name, bases, attrs)
            cls.__module__ != '__fake__' and BaseMeta.models.append(cls)
            return cls

            # # Authorization tables:
            # class auth:
            #     access = RangeDict()
            #     create = RangeDict()
            #     modify = RangeDict()
            #     serializer = None
            #     # objects = DBAuthModelManager()  # todo
            #
            # attrs['auth'] = auth
        else:
            cls = super().__new__(mcs, name, bases, attrs)
            return cls

    def __init__(cls, name, bases, attrs):
        super().__init__(name, bases, attrs)

        if cls in BaseMeta.models:
            if 'class_init' in cls.__dict__:
                raise AttributeError(
                    "ModelMixin classmethod 'class_init' should only be defined in abstrat mixin classes"
                )

            invoked = set()
            def invoke(base):
                nonlocal cls
                if 'class_init' not in base.__dict__:
                    [invoke(b) for b in base.__bases__ if issubclass(base, ModelMixin)]
                elif base not in invoked:
                    invoked.add(base)
                    base.class_init.__func__(cls)

            [invoke(base) for base in bases if issubclass(base, ModelMixin)]

            # # Serializer:
            # Serializer = serializer_factory(cls)
            # AuthSerializer = serializer_factory(cls, auth=True)
            #
            # # Fields hidden by default:
            # for exclusion in ['created', 'modified', 'deleted']:
            #     try:
            #         cls._meta.get_field(exclusion) and Serializer.Meta.exclude.append(exclusion)
            #     except FieldDoesNotExist:
            #         pass
            #
            # # Parse serializer options:
            # serializer_options = attrs.get('Serializer', object)
            # for option, value in serializer_options.__dict__.items():
            #     if option.startswith('_'):  # Not an option
            #         pass
            #     elif option == 'exclude':
            #         Serializer.Meta.exclude += value
            #     else:
            #         setattr(Serializer.Meta, option, value)
            # cls.serializer = Serializer
            # cls.auth.serializer = AuthSerializer


class ModelMixin(metaclass=BaseMeta):
    @abstractmethod
    def __init__(self, *args, **kwargs): ...

    @staticmethod
    def class_attributes(**kwargs) -> None:
        """ Exposes class attributes before instantiation of the model class.

        Keyword Args:
            attrs (dict): Class attributes.
            name (str): Name of class.
            meta (type): The Meta class of the model.
            super_attributes(() -> None): A shorthand callable that invokes each super classes' class_attributes.
        """

    @classmethod
    def class_init(cls):
        ...


# # noinspection PyRedeclaration
# class BaseModel(ABC, Model, metaclass=BaseMeta):
#     class Meta:
#         abstract = True
#

class TrueName(ModelMixin, ABC):
    """ Automatically deduce snake_case_table name from UpperCaseModel class name, unless manually defined in Meta.
    Also sets default related name to the table name.
    """

    table_name_reg = re.compile(r'(?!^)(?<!_)([A-Z])')

    @staticmethod
    def class_attributes(attrs, name, meta, **kwargs):
        if not hasattr(meta, 'db_table'):
            meta.db_table = TrueName.table_name_reg.sub(r'_\1', name).lower()
        meta.default_related_name = meta.db_table


class FieldTracking(ModelMixin, ABC):
    tracker = ...  # type: FieldTracker

    @staticmethod
    def class_attributes(attrs, **kwargs):
        attrs['tracker'] = FieldTracker()


class Unmanaged(ModelMixin, ABC):  # todo: is this useful?
    @staticmethod
    def class_attributes(meta, **kwargs):
        meta.managed = False


class Accessors(ModelMixin, ABC):
    def __getitem__(self, item):
        return getattr(self, item)

    def __setitem__(self, key, value):
        return super().__setattr__(key, value)

    def set_value(self, key, value):
        return super().__setattr__(key, value)

    def get_values(self, keys):
        values = dict.fromkeys(keys)
        for k in keys:
            k_path = k.split('__')  # Handles relation access
            value = self
            for k_part in k_path:
                value = getattr(value, k_part)
            values[k] = value
        return values


class Eventful(FieldTracking, Accessors, ABC):
    event_hooks = ...  # type: EventHooks

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.number_of_saves = 0

    @classmethod
    def class_init(cls):
        ...  # todo:
        # cls.event_hooks = EventHooks(attr.as_hook(cls) for attr in attrs.values() if isinstance(attr, event))

    def save(self, *args, values: dict=None, **kwargs):
        # from .events import BEFORE, ON_CREATE, ON_CHANGE  # todo: replace below placeholders
        BEFORE, ON_CREATE, ON_CHANGE = 1, 2, 3

        self.number_of_saves += 1
        values and [self.set_value(f, v) for f, v in values.items()]
        changes = self.tracker.changed()  # todo: touch-event

        if changes and self.event_hooks:
            is_update = self.tracker.previous(self._meta.pk.name) is not None
            event_type = ON_CHANGE if is_update else ON_CREATE

            with self.event_hooks.consumer_context(self) as consumer:
                consumer.call(BEFORE | event_type)
                ret = super().save(*args, **kwargs)
                consumer.call(event_type)
            return ret
        else:
            return super().save(*args, **kwargs)


#
# class _ModelMeta(ModelBase):
#     def __init__(cls: Type['DBModel'], name, bases, attrs):
#         # Sets up composition instance that require the Model class
#         if cls in models:
#             # Serializer:
#             Serializer = serializer_factory(cls)
#             AuthSerializer = serializer_factory(cls, auth=True)
#
#             # Hooks:
#             cls.event_hooks = EventHooks(attr.as_hook(cls) for attr in attrs.values() if isinstance(attr, event))
#
#             # Fields hidden by default:
#             for exclusion in ['created', 'modified', 'deleted']:
#                 try:
#                     cls._meta.get_field(exclusion) and Serializer.Meta.exclude.append(exclusion)
#                 except FieldDoesNotExist:
#                     pass
#
#             # Parse serializer options:
#             serializer_options = attrs.get('Serializer', object)
#             for option, value in serializer_options.__dict__.items():
#                 if option.startswith('_'):  # Not an option
#                     pass
#                 elif option == 'exclude':
#                     Serializer.Meta.exclude += value
#                 else:
#                     setattr(Serializer.Meta, option, value)
#             cls.serializer = Serializer
#             cls.auth.serializer = AuthSerializer
#
#         super().__init__(name, bases, attrs)
#
#     # #
#     # Convenience:
#     def reverse_relations(cls):
#         return [f.name for f in cls._meta.get_fields() if f.auto_created and not f.concrete]
#
#
# class DBModel(Model, metaclass=_ModelMeta):
#     class Meta:
#         abstract = True
#
#     objects = DBModelManager()
#     models = DBAuthModelManager()
#     serializer = None  # type: Type[DBModelSerializer]
#     auth = None
#     tracker = None  # type: FieldTracker
#
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.number_of_saves = 0
#
#     @classmethod
#     def where(cls, query=None) -> QuerySet:
#         return cls.objects.where(query)
#
#     # #
#     # Convenience:
#
#
#     #  #
#     # Overrides:
#     _need_auth = False
