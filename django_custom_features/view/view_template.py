from abc import ABC
from typing import Iterable, TYPE_CHECKING

from django.http import HttpResponse
from django.views import View

if TYPE_CHECKING:
    from .router import Router, RouterDoc


class ViewTemplate(ABC):
    """
    Base class for view templates. Any subclass of TemplateView must explicitly declare a HTTP method handler by
    overriding its parent's method. If the method is not overriden then it will not be available for the view.

    An instance of a TemplateView can be used as a View from its view property.
    """

    def __init__(self, route, methods, **kwargs):
        """
        Args:
            route (str):
            methods (Iterable[str]): Allowed methods.
        """
        super().__init__(**kwargs)
        self.route = route

        methods = {method.lower() for method in methods}

        # Disallow HTTP method if subclass does not override it explicitly:
        def subclass_defines(method):
            callback = getattr(self.__class__, method)
            return callback != getattr(self.__class__.__base__, method, callback)

        self.available_methods = {method for method in methods if subclass_defines(method)}
        self.enabled_methods = methods & self.available_methods
        self.disabled_methods = set(View.http_method_names) - self.enabled_methods

        unknown_metods = methods - set(View.http_method_names)
        if unknown_metods:
            raise ValueError('View defined with unknown HTTP method(s): %s.' % ','.join(unknown_metods))

    @property
    def view(self):
        class ViewInstance(View):
            ...

        for k, v in self.__dict__.items():
            if not k.startswith('__') and not hasattr(ViewInstance, k):
                setattr(ViewInstance, k, v)  # todo: fuzzy logic, find better way to actualize template view?

        for method in self.enabled_methods:
            setattr(ViewInstance, method, getattr(self.__class__, method))

        return ViewInstance

    def get(self, *args) -> HttpResponse:
        ...

    def delete(self, *args) -> HttpResponse:
        ...

    def patch(self, *args) -> HttpResponse:
        ...

    def post(self, *args) -> HttpResponse:
        ...

    def put(self, *args) -> HttpResponse:
        ...

    def doc(self, docs: 'RouterDoc') -> dict:
        ...

    def add_to(self, router: 'Router', **kwargs):
        if self.enabled_methods:
            router.route(self.route, self.view, **kwargs)
            self.doc(router.doc)
