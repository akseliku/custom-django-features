import re
from datetime import datetime

from django.shortcuts import render
from django.urls import re_path
from django.views import View

# from typing import Type, Sequence
# from .template_view import *
# from .template_doc import *

from django_custom_features.util import find_or_create

# _modeltype = Type[DBModel]
# _modeltypes = Sequence[_modeltype]


class Router:
    instances = []
    types_as_regex = {'str': r'\w+', 'int': r'\d+'}

    def __init__(self, *middlewares):
        self.instances.append(self)

        self._doc = RouterDoc()

        self._middlewares = middlewares
        self._urlpatterns = []
        self._name = None

    @property
    def name(self):
        return self._name

    @property
    def urlpatterns(self):
        return self._urlpatterns

    @property
    def doc(self):
        return self._doc

    def document(self, route: str, template: str, name: str):
        """Creates documentation view."""
        if name in [r.name for r in self.instances if r is not self]:
            raise ValueError('Different routers cannot have same doc name "%s"' % name)
        self._name = name
        self(route)(lambda req: render(req, template))

    def format_route(self, route, **kwargs) -> str:
        """

        Args:
            route(str): The unformatted route, as it is passed to router.

        Returns:
            The formatted route.
        """
        # Substitute path variables:
        route = re.sub(r'<(\w+):(\w+)>', lambda m: r'(?P<%s>%s)' % (m[2], self.types_as_regex[m[1]]), route)
        # Normalize leading/trailing slash:
        route = '^%s/?$' % route.strip('/')
        return route

    def __call__(self, route=None, method=None, **kwargs):
        """
        Register a route using instance as decorator.
        """

        def decorator(cls_or_func):
            nonlocal route, kwargs
            if route is None:
                route = cls_or_func.__name__.lower() + '/'

            self.route(route, cls_or_func, method, **kwargs)
        return decorator

    def route(self, route, cls_or_func, method=None, **kwargs) -> None:
        """

        Args:
            route (str): The URL path. Use <type:parameter> format for path-paramters, or override Router.format_route()
                to customize route formatting. By default result path will accept
            cls_or_func(View | callable): The view.
            method (str): Allow only specified HTTP method, if routed view is a function.
        """

        route = self.format_route(route, **kwargs)

        if isinstance(cls_or_func, type) and issubclass(cls_or_func, View):
            cls_or_func = cls_or_func.as_view()
            if method:
                raise ValueError("Class views should not have method restrictions")
        elif method:
            # Restrict method by wrapping in view class:
            method = method.lower()
            if method not in ('get', 'delete', 'patch', 'post', 'put'):
                raise ValueError('Unknown HTTP method "%s"' % method)
            class view(View): pass
            cb = cls_or_func
            def method_wrapper(_self, *a, **kw):
                return cb(*a, **kw)
            setattr(view, method, method_wrapper)
            cls_or_func = view.as_view()

        for middleware in self._middlewares:
            cls_or_func = middleware(cls_or_func)
        self._urlpatterns.append(re_path(r'%s' % route, cls_or_func))


class RouterDoc:
    def __init__(self):
        self._doc = {
            'openapi': '3.0.1',
            'info': {
                'title': 'P100',
                'version': datetime.now().strftime('%Y.%j.%H'),
                'description': 'Test',
            },
            'paths': {},
            'tags': [],
            'x-tagGroups': [
                {'name': 'Models', 'tags': []},
                {'name': 'Relations', 'tags': []},
            ],
        }

    def get(self) -> dict:
        return self._doc

    def add_path(self, path, path_doc):
        self._doc['paths'][path] = path_doc

    def add_tag(self, tag: str, description: str=None, group: str=None):
        tags, tag_groups = self._doc['tags'], self._doc['x-tagGroups']

        tag_doc = find_or_create(tags, 'name', tag)

        if description:
            tag_doc['description'] = description

        if group:
            group_doc = find_or_create(tag_groups, 'name', group, defaults={'tags': []})
            tag not in group_doc['tags'] and group_doc['tags'].append(tag)
